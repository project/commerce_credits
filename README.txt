Commerce Credits
----------------

Description
-----------

Commerce Credits allows you to have users buy or win credits. The credits these
users buy or win are fully configurable and can expire after some time. The
process used for these credits is also fully configurable through Rules. It
allows you to entirely define your own process for the payment and the use of
these credits.

Installation
------------

- Copy the module's directory to your modules directory and activate the module.

Usage
-----

Create Credit Types by going to admin/commerce/credits/types. Set a Name,
Description and Duration for the credit. The duration is utilized by cron to
determine the length of the applicable action.

Create Commerce Products utilizing these Credit Types at
admin/commerce/products/add/commerce-credits. Choose a SKU, Title, Number of
credits included in product purchase, Price, and the Credit type created from
the previous step.

Create a Content Type to display your product admin/structure/types/add.

Add a product reference field to the new Content Type, allowing only Commerce
Credits as product types that can be referenced.

Create a new piece of content of your new product content type.

Now, create Rules to implement required functionality.

Events defined:
- After credit choice
- After deleting a commerce credit
- After saving a new commerce credit
- After updating an existing commerce credit
- Before saving a commerce credit
- When credits expire

Actions defined:
- Add credits
- Delete chosen credit
- Remove credits
- Set credits expiration date
- Transfer credits
- Updates credits based on order

